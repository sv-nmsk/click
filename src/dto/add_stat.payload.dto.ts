import { IsEmail, IsString, Length, IsEnum, IsArray, IsNumber, IsInt } from 'class-validator';

export class StatAdd {
  @IsArray()
  items: [{key: string, value: string}];

  @IsInt()
  count: number;
}
