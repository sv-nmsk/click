import { Injectable } from '@nestjs/common';
import { ClickhouseService } from '@flyhigh-hifi/clickhouse';

@Injectable()
export class ClickService {
  constructor(private readonly ch: ClickhouseService) {}

  async list() {
    const res = this.ch.query(`
      SELECT service_id, count(email) as count
      FROM stats.login
      GROUP BY service_id
      ORDER BY count
    `);
    res.subscribe(console.log);
    return res;
  }

  async add(items: [object]): Promise<string> {
    console.log(items);
    return new Promise<string>((resolve) => {
      setTimeout(() => {
        resolve('done');
      }, 100);
    });
  }
}
