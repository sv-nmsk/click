import { Module } from '@nestjs/common';
import { ClickService } from './click.service';
import { ClickController } from './click.controller';
import { ClickhouseModule } from '@flyhigh-hifi/clickhouse';

@Module({
  imports: [
    ClickhouseModule.register({
      url: 'http://localhost',
      port: 8123,
      debug: false,
    }),
  ],
  providers: [ClickService],
  controllers: [ClickController],
})
export class ClickModule {}
