import { Body, Controller, Get, Post } from '@nestjs/common';
import { ClickService } from './click.service';
import * as DTO from '../dto';

@Controller('click')
export class ClickController {
  constructor(private readonly clickService: ClickService) {}

  @Get('/list')
  async list() {
    return await this.clickService.list();
  }

  @Post('/add')
  async add(@Body() data: DTO.StatAdd): Promise<string> {
    return await this.clickService.add(data.items);
  }
}
