import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClickModule } from './click/click.module';
// import { ClickhouseModule } from '@flyhigh-hifi/clickhouse';

@Module({
  imports: [
    // ClickhouseModule.register({
    //   url: 'http://localhost',
    //   port: 8123,
    //   debug: false,
    // }),
    ClickModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
